import {GraphQLID, GraphQLNonNull, GraphQLObjectType, GraphQLString} from 'graphql';
import UserEntity from "../../entities/UserEntity";
import {UserType} from "./UserSchema";

export const PostType: any = new GraphQLObjectType({
    name: 'Post',
    fields: () => ({
        id: { type: new GraphQLNonNull(GraphQLID) },
        title: { type: new GraphQLNonNull(GraphQLString) },
        content: { type: new GraphQLNonNull(GraphQLString) },
        author: {
            type: UserType,
            resolve: async (parent) => {
                return UserEntity.findById(parent.user);
            },
        },
    }),
});