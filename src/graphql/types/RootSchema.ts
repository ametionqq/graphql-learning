import {GraphQLID, GraphQLList, GraphQLNonNull, GraphQLObjectType, GraphQLSchema, GraphQLString} from 'graphql';
import UserEntity from "../../entities/UserEntity";
import PostEntity from "../../entities/PostEntity";
import {PostType} from "./PostSchema";
import {UserType} from "./UserSchema";

const RootQueryType = new GraphQLObjectType({
    name: 'RootQuery',
    fields: () => ({
        user: {
            type: UserType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
            },
            resolve: async (parent, args) => {
                return UserEntity.findById(args.id);
            },
        },
        users: {
            type: new GraphQLList(UserType),
            resolve: async () => {
                return UserEntity.find();
            },
        },
        post: {
            type: PostType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
            },
            resolve: async (parent, args) => {
                return PostEntity.findById(args.id);
            },
        },
        posts: {
            type: new GraphQLList(PostType),
                resolve: async () => {
                    return PostEntity.find();
            },
        },
    }),
});

const RootMutationType = new GraphQLObjectType({
    name: 'RootMutation',
    fields: () => ({
        createUser: {
            type: UserType,
            args: {
                firstName: { type: new GraphQLNonNull(GraphQLString) },
                secondName: { type: new GraphQLNonNull(GraphQLString) },
                age: { type: new GraphQLNonNull(GraphQLString) },
                login: { type: new GraphQLNonNull(GraphQLString) },
                password: { type: new GraphQLNonNull(GraphQLString) },
                email: { type: new GraphQLNonNull(GraphQLString) },
            },
            resolve: async (parent, args) => {
                const user = new UserEntity(args);
                await user.save();
                return user;
            },
        },
        createPost: {
            type: PostType,
            args: {
                title: { type: new GraphQLNonNull(GraphQLString) },
                content: { type: new GraphQLNonNull(GraphQLString) },
                author: { type: new GraphQLNonNull(GraphQLID) },
            },
            resolve: async (parent, args) => {
                const { author, ...postData } = args;
                const post = new PostEntity(postData);
                post.author = author;
                await post.save();

                return PostEntity.findById(post._id).populate('author');
            },
        },
    }),
});

export const schema = new GraphQLSchema({
    query: RootQueryType,
    mutation: RootMutationType,
});