import {GraphQLID, GraphQLNonNull, GraphQLObjectType, GraphQLString} from 'graphql';

export const UserType: any = new GraphQLObjectType({
    name: 'User',
    fields: () => {
        return {
            id: {type: new GraphQLNonNull(GraphQLID)},
            firstName: { type: new GraphQLNonNull(GraphQLString) },
            secondName: { type: new GraphQLNonNull(GraphQLString) },
            age: { type: new GraphQLNonNull(GraphQLString) },
            login: { type: new GraphQLNonNull(GraphQLString) },
            password: { type: new GraphQLNonNull(GraphQLString) },
            email: { type: new GraphQLNonNull(GraphQLString) },
        };
    },
});