import { model, Schema } from 'mongoose';

const UserEntity = new Schema({
    firstName: {type: String, required: true},
    secondName: {type: String, required: true},
    age: {type: Number, required: true},
    login: { type: String, required: true },
    password: {type: String, required: true},
    email: { type: String, required: true },
});

export default model('User', UserEntity);
