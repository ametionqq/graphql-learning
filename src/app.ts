import express from "express"
import mongoose from "mongoose";
import {graphqlHTTP} from "express-graphql";
import {schema} from "./graphql/types/RootSchema";

require("dotenv").config();

const app = express()

mongoose.connect(process.env.mongoLink!);

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true,
}));

app.listen(5000, () => {
    console.log("started")
})